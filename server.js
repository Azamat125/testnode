require('dotenv').config({ path: './.env' });
const express = require('express');
const mainRouter = require('./src/route/main.route');

const app = express();
app.use(express.json());
const port = process.env.BACK_PORT;
app.use(mainRouter);
const startServer = () => {
  try {
    app.listen(port, () => {
      console.log(`server started on port: ${port}`);
    });
  } catch (e) {
    console.log(e);
  }
};

startServer();
