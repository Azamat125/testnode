const userActions = require('../../../userActions/userActions');

const getUsersList = async (req, res) => {
  const userList = await userActions.getUsersList();
  try {
    return res.send(userList);
  } catch (e) {
    return res
      .status(401)
      .send({ message: 'Ошибка вывода списка пользователей', content: e });
  }
};

module.exports = getUsersList;
