const { nanoid } = require('nanoid');
const userActions = require('../../../userActions/userActions');

const registrationUser = async (req, res) => {
  try {
    const { name } = req.body;
    if (!name) {
      return res.status(401).send({
        message: 'Ошибка регистрации пользователя',
        content: 'Поле Имя пустое',
      });
    }
    const token = nanoid(10);
    userActions.createUser({ id: nanoid(4), name, token, purse: 0 });
    return res.status(200).send({ token });
  } catch (e) {
    return res
      .status(401)
      .send({ message: 'Ошибка регистрации пользователя.', content: e });
  }
};

module.exports = registrationUser;
