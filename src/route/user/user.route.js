const { Router } = require('express');
const getUsersList = require('./controllers/getUsersList');
const registrationUser = require('./controllers/registrationUser');

const userRouter = Router();

userRouter.post('/', registrationUser);
userRouter.get('/', getUsersList);

module.exports = userRouter;
