const { Router } = require('express');
const promotionRouter = require('./promotion/promotion.route');
const userRouter = require('./user/user.route');

const mainRouter = Router();

mainRouter.use('/user', userRouter);
mainRouter.use('/promo', promotionRouter);
module.exports = mainRouter;
