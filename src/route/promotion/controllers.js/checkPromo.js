let userActions = require('../../../userActions/userActions');

const checkPromo = async (req, res) => {
  try {
    const { token, ownerTokenID } = req.body;
    if (!token || !ownerTokenID) {
      return res.status(401).send({
        message: 'Ошибка ввода промо-кода',
        content: 'Пустые поля',
      });
    }
    let { users } = userActions.getUsersList();

    let checkToken = users.filter((user) => user.token === token);
    if (checkToken.length === 0) {
      return res.status(401).send({
        message: 'Ошибка ввода промо-кода',
        content: 'Код не корректен',
      });
    }

    let newUsers = users.map((user) => {
      if (user.id === ownerTokenID) {
        return { ...user, purse: user.purse + 300 };
      }
      if (user.token === token) {
        return { ...user, purse: user.purse + 300 };
      }
      return user;
    });

    userActions.changePurseUser(newUsers);

    return res.status(200).send(await userActions.getUsersList());
  } catch (e) {
    return res
      .status(401)
      .send({ message: 'Ошибка ввода промо-кода', content: e });
  }
};

module.exports = checkPromo;
