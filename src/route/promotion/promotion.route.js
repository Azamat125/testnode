const { Router } = require('express');
const checkPromo = require('./controllers.js/checkPromo');

const promotionRouter = Router();

promotionRouter.post('/', checkPromo);

module.exports = promotionRouter;
