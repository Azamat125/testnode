const fs = require('fs');
const pathFile = __dirname + '/db.json';

const userActions = {
  createUser: (dataUser) => {
    let userList = userActions.getUsersList();
    userList.users.push(dataUser);
    fs.writeFileSync(pathFile, JSON.stringify(userList));
  },
  getUsersList: () => {
    const list = fs.readFileSync(pathFile);
    return JSON.parse(list);
  },
  changePurseUser: (data) => {
    fs.writeFileSync(pathFile, JSON.stringify({ users: [...data] }));
  },
};
module.exports = userActions;
